-- Variáveis registro default --
set @taskTitle = 'Atividade. Edite, finalize ou exclua este registro';
set @taskDescription = 'Edite, finalize ou exclua este registro';
set @createdAt = NOW();
set @isDone = FALSE;

CREATE TABLE IF NOT EXISTS task (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  title varchar(50) NOT NULL,
  long_description longtext NOT NULL,
  created_date datetime NOT NULL DEFAULT NOW(),
  finish_date datetime NULL DEFAULT NULL,
  is_done boolean NOT NULL DEFAULT FALSE,
  due_date datetime NULL DEFAULT NULL,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

INSERT INTO task (title, long_description , created_date , is_done) VALUES
( @taskTitle, @taskDescription, @createdAt , @isDone );