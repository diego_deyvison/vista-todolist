jQuery(function ($) {
    $('#finish_date').datetimepicker({
        format:'YYYY-MM-DD hh:mm:00'
    });
});


jQuery(document).ready(function($){

    $('.delete-action').on('click', function(e) {
        e.preventDefault();

        var destUrl = $(this).attr('href');

        $('#confirm').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        })
        .one('click', '#delete', function(e) {
            window.location = destUrl;
        });
    });
    
});