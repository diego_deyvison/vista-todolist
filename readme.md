Lista de tarefas "To do" - Desafio Técnico Programador PHP - Vista Software

Desafio: Criar uma "Lista de Tarefas (To Do)" com as seguintes funcionalidades: Adicionar, Listar, Editar e Excluir. O código deve seguir os padrões MVC sendo Orientado a Objeto. A linguagem a ser utilizada deve ser o PHP (sem a utilização de frameworks) como backend.

---
Deploy:
1. Baixar o repositório;
2. Importar o arquivo "tasks.sql" na raiz do projeto para o banco de dados do servidor;
3. Atualizar o arquivo "config.php" na raiz do projeto com os dados de banco de dados e a URL base da aplicação;