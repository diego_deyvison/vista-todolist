<?php 
$title = !isset($title)?'Lista de tarefas - To do' : $title;
require_once dirname(__FILE__).'/header.php';
?>
<div class="container">
    <h1><?php print htmlentities($title) ?></h1>
    <p>
        <?php print htmlentities($message) ?>
    </p>
</div>
    </body>
</html>
<?php require_once dirname(__FILE__).'/footer.php'; ?>
