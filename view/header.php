<!DOCTYPE html>
<html>
    <head>
        <title><?php print $title ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="<?php echo BASEURL ?>static/css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
    </head>
    
    <body>
        <header id="head">      
            <div class="container">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php print BASEURL ?>">To do List</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="<?php print $action == 'list' && $status == 'open' ? 'active' : '';?>"><a href="<?php print BASEURL. '?action=list&status=open' ?>">Tarefas para fazer</a></li>
                                <li class="<?php print $action == 'list' && $status == 'closed' ? 'active' : '';?>"><a href="<?php print BASEURL.'?action=list&status=closed' ?>">Tarefas concluídas</a></li>
                                <a href="<?php print BASEURL.'?action=new'; ?>" class="btn btn-default navbar-btn btn-success">Nova tarefa</a>
                            </ul>
                        </div>
                    <div>
                </nav>
            </div>
        </header>
