<?php 
$title = !isset($title)?'Lista de tarefas - To do' : $title;
require_once dirname(__FILE__).'/../header.php';
?>

<div class="container">
    <div class="row">

        <h1 class="h1 col-md-12"><?php print $title ?></h1>
    
        <form id="task-form" class="col-md-8" method="POST" action="">

            <div class="form-group">
                <label for="title">Título *</label>
                <input required="true" class="form-control" type="text" id="title" name="title" value="<?php print htmlentities($taskTitle) ?>"/>
            </div>
                    
            <div class="form-group">
                <label for="long_description">Descrição da tarefa *</label>
                <textarea required="true" class="form-control" rows="3" name="long_description" id="long_description"><?php print htmlentities($taskDescription) ?></textarea>
            </div>

            <div class="form-group">
                <label for="finish_date">Data para finalizar</label>
                <div class='input-group date' id='finish_date'>
                    <input type='text' name="finish_date" value="<?php print $taskFinishDate ?>" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="finish_date">Status da tarefa: <?php print $taskIsDone == true ? 'Finalizada' : 'Em aberto'; ?></label>
                <input class="form-control" name="id" id="id" value="<?php print $taskId; ?>" type="hidden">
                <input class="form-control" name="is_done" id="is_done" value="<?php print $taskIsDone; ?>" type="hidden">
                <input class="form-control" name="due_date" id="due_date" value="<?php print $taskDueDate; ?>" type="hidden">
                <input class="form-control" name="created_date" id="created_date" value="<?php print $taskCreatedDate; ?>" type="hidden">
            </div>
            
            <input type="hidden" name="form-submitted" value="1" />
            <input type="submit" class="btn btn-success" value="Salvar" />
            <a href="<?php print BASEURL ?>" class="btn btn-default">Cancelar</a>
        </form>

        <div class="clear clearfix"></div>

        <?php if(isset($msgSucess)): ?>
            <div class="success-message alert alert-success">
                <strong><?php print $msgSucess; ?></strong>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php require_once dirname(__FILE__).'/../footer.php'; ?>