<?php 

$title = 'Lista de tarefas - To do';
require_once dirname(__FILE__).'/../header.php';

?>

<div class="container">
        <table class="table table-responsive table-hover">
        <thead>
            <tr>
            <th>ID</th>
            <th>Tarefa</th>
            <th>Data para conclusão</th>
            <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($tasks)): ?>

                <?php foreach($tasks as $task): ?>

                    <tr>
                        <th scope="row"><?php print htmlentities($task['id'])?></th>
                        <th><a title="<?php print 'Editar: '.$task['title']; ?>" href="<?php print BASEURL .'?action=edit&task='.$task['id']?>"><?php print htmlentities($task['title'])?></a></th>
                        <th><?php print !is_null($task['finish_date']) ? htmlentities($task['finish_date']) : '-' ?></th>
                        <td>
                            <a href="<?php print BASEURL .'?action=edit&task='.$task['id']?>" class="btn btn-primary">Editar</a>
                            <?php if($task['is_done'] == true): ?>
                                <a href="<?php echo BASEURL.'?action=reopen&id='.$task['id']?>" type="button" class="btn btn-success"><?php print 'Reabrir'?></a>
                            <?php else: ?>
                                <a href="<?php echo BASEURL.'?action=finish&id='.$task['id']?>" type="button" class="btn btn-success"><?php print 'Finalizar'?></a>
                            <?php endif;?>
                            <a href="<?php echo BASEURL.'?action=delete&id='.$task['id']?>" class="btn btn-danger delete-action">Excluir</button>
                        </td>
                    </tr>

                <?php endforeach;?>
                
            <?php endif; ?>

        </tbody>
        </table>
    
</div>

<div id="confirm" class="modal fade">
  <div class="modal-body">
    Tem certeza que deseja apagar o registro?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Deletar</button>
    <button type="button" data-dismiss="modal" class="btn">Cancelar</button>
  </div>
</div>


<?php

require_once dirname(__FILE__).'/../footer.php';

?>