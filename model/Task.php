<?php 

class Task{

    private $dbLink;
    private $database_host;
    private $database_user;
    private $database_password;
    private $database_schema;

    public function __construct(){
        $this->database_host = DB_HOST;
        $this->database_user = DB_USER;
        $this->database_password = DB_PASSWORD;
        $this->database_schema = DB_SCHEMA;
    }

    /**
     * Abre uma conexão com o mysql
     */
    private function openDb() {

        $this->dbLink = new mysqli($this->database_host , $this->database_user  , $this->database_password  );
        
        if (mysqli_connect_errno()) {
            throw new Exception("Erro ao conectar no banco de dados");
            exit();
        }
        
        if (!$this->dbLink->select_db($this->database_schema)) {
            throw new Exception("Não foi possível encontrar o banco de dados todolist");
        }

    }

    private function closeDb(){

        $this->dbLink->close();

    }

    /**
     * Lista as tarefas paginadas
     */
    public function listTasks(array $where = array() , array $orderby = array() , $limit = NULL, $offset = 0){
        return $this->select($where , $orderby , $limit , $offset);
    }

    /**
     * Retorna uma task pelo ID
     */
    public function getTask($id){

        $task = $this->select(array('id' => $id));

        if(empty($task))
            throw new Exception("Não existe tarefa associada ao ID $id");

        if(!empty($task) and is_array($task)){
            return $task[0];
        }

    }

    /**
     * Cria uma task no banco
    */
    public function createTask(array $data){
        
        $result = $this->insert($data);

        if(!$result)
            throw new Exception("Houve um erro ao gravar os dados");

        return $result;
        
    }

    /**
     * Atualiza uma task no banco
     */
    public function updateTask(array $data){

        $result = $this->update($data);

        if(!$result)
            throw new Exception("Não foi possível atualizar o registro");
        
        return $result;

    }


    /**
     * Deleta uma ou várias task no banco
     */
    public function deleteTask($id){

        $result = $this->delete($id);

        $count = is_array($id) ? count($id) : 1;

        if($result !== $count)
            throw new Exception("Um ou mais registros não puderam ser deletados");

        return true;

    }

    /**
     * formata o select de acordo com os criterios passados
     */
    private function select(array $where = array() , array $orderby = array() , $limit = NULL , $offset = 0){

        $query = 'SELECT * FROM task ';

        if(!empty($where)){
            $query .= $this->selectSetCreteria($where , 'where');
        }

        if(!empty($orderby)){
            $query .= $this->selectSetCreteria($orderby , 'orderby');
        }

        if($limit and !is_null($limit)){
            $query .= 'LIMIT '.$limit.' ';
        }

        if($offset > 0){
            $query .= 'OFFSET '.$offset.' ';
        }

        $query .= ';';

        $this->openDb();
        $result = $this->queryDb($query , 'select');
        $this->closeDb();

        return $result;

    }

    /**
     * Formata a string da query com os criterios de Order By e Where
    */
    private function selectSetCreteria(array $values , $context){

        $query = '';
        $count = count($values);
        $i = 0;

        if($context == 'where'){
            $query.= 'WHERE ';
        }else{
            $query.= 'ORDER BY ';
        }

        foreach($values as $key => $value){

            if($context == 'where'){
                $query.= $key.' = "'.$value.'" ';
            }else{
                $query.= $key.' '.$value.' ';
            }

            $i++;

            if($count > $i ){
                $query.= ', ';
            }

        }

        return $query;
        
    }

    /**
     * Insere um registro de task no banco
     */
    private function insert( array $data){

        $this->openDb();
        $data = $this->filterArrayData($data);

        $query = 'INSERT INTO task ';
        $keys = '( '.implode (' ,' , array_keys($data)).' )';
        $values = '(';

        $i = 0;
        $count = count(array_values($data)); 

        foreach(array_values($data) as $value){

            $values .= is_null($value) ? ' NULL ' : '"'.$value.'"';

            $i++;
            if($count > $i)
                $values .= ',';

        }

        $values .= ')';

        $query .= $keys.' VALUES '.$values.';';

        $result = $this->queryDb($query , 'insert');
        $this->closeDb();

        return $result;

    }

    /**
     * Remove as colunas "nulladas"
     */
    private function filterArrayData(array $data){

        foreach($data as $key => $value){
            
            if(is_null($value)){
                $data[$key] = NULL;
            }else{
                $data[$key] = $this->dbLink->real_escape_string($value);
            }
            
        }

        return $data;

    }

    /**
     * atualiza um registro de task no banco
     */
    private function update( array $data){

        $this->openDb();

        $data = $this->filterArrayData($data);
        $id = $data['id'];
        unset($data['id']);

        $query = 'UPDATE task SET ';

        $i = 0;
        $count = count($data);

        foreach($data as $key => $value){
            
            if(!is_null($value)){ 
                $query .= $key.' = "'.$value.'"';
            }else{
                $query .= $key.' = NULL';
            }
            
            $i ++;
            
            if($count > $i)
                $query .= ',';

        }

        $query .= ' WHERE id = '.$id. ';';

        $result = $this->queryDb($query , 'update');
        $this->closeDb();

        return $result;

    }

    /**
     * remove um registro de task do banco
     */
    private function delete ($id){

        if(!is_array($id)){

            $query = 'DELETE from task where id='.$id.';';

        }else{

            $query = 'DELETE from task where id IN ('.implode(',' , $id).');';

        }
        
        $this->openDb();
        $result = $this->queryDb($query , 'delete');
        $this->closeDb();

        return $result;

    }

    /**
     * interage com o banco de dados de acordo com a query passada
     */
    private function queryDb($query , $query_type = 'select'){
        
        $return = '';

        $result = $this->dbLink->query($query);

        if($query_type == 'insert'){
            $return =  $this->dbLink->insert_id;
        }

        if($query_type == 'update'){
            $return = $result;
        }

        if($query_type == 'delete'){
            $return = $this->dbLink->affected_rows;
        }
    
        if($query_type == 'select'){
            
            $return = array();
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $return[] = $row;
            }

        }

        return $return;

    }

}

?>