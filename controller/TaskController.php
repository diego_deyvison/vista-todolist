<?php 

require_once 'model/Task.php';

class TaskController{

    private $taskService = NULL;

    public function __construct(){
        $this->taskService = new Task();
    }

    public function redirect($location){
        header('Location: '.$location);
    }

    /**
     * Router de ações do sistema
     */
    public function handleRequest() {
        $action = isset($_GET['action'])?$_GET['action']:NULL;
        try {
            if ( !$action || $action == 'list' ) {
                $status = isset($_GET['status']) && $_GET['status'] == 'closed' ?$_GET['status']:'open';
                $this->listTasks($status);
            } elseif ( $action == 'new' ) {
                $this->editTask($action);
            } elseif ( $action == 'edit' ) {
                $this->editTask($action);
            } elseif ( $action == 'delete' ) {
                $this->deleteTask();
            } elseif ( $action == 'finish' ) {
                $this->finishTask();
            } elseif ( $action == 'reopen' ) {
                $this->reopenTask();
            } else {
                $this->showError("Página não encontrada", "A página para a ção".$action." não existe!");
            }
        } catch ( Exception $e ) {
            // some unknown Exception got through here, use application error page to display it
            $this->showError("Erro da aplicação", $e->getMessage());
        }
    }

    /**
     * carrega no front a listagem de atividades de acordo com o status
     */
    private function listTasks($status = 'open'){
        $args = array();

        if($status == 'closed'){
            $args['is_done'] = 1;
        }else{
            $args['is_done'] = 0;
        }
        
        $tasks = $this->taskService->listTasks($args); 
        $action = 'list';
        
        include 'view/task/task.list.php';
    }

    /**
     * carrega no front o formulario de edição de uma tarefa
     */
    private function editTask($action = 'new'){

        $taskId = '';
        $taskTitle = '';
        $taskDescription = '';
        $taskFinishDate = '';
        $taskCreatedDate = '';
        $taskIsDone = '';
        $taskDueDate = '';
        
        if($action == 'new'){

            $title = 'Adicionar nova tarefa';
            $taskIsDone = 0;

        }else if($action == 'edit'){

            if(!isset($_GET['task']) or is_null($_GET['task']))
                throw new Exception('Internal error.');

            if(isset($_GET['save']) and $_GET['save'] == 'success')
                $msgSucess = "Tarefa salva com sucesso";
            
            $id = $_GET['task'];

            $task =  $this->taskService->getTask($id);
            
            $title = 'Editar tarefa - '.$task['title'];

            $taskId = $task['id'];
            $taskTitle = $task['title'];
            $taskDescription = $task['long_description'];
            $taskFinishDate = $task['finish_date'];
            $taskCreatedDate = $task['created_date'];
            $taskIsDone = $task['is_done'];
            $taskDueDate = $task['due_date'];

        }

        if ( isset($_POST['form-submitted']) ) {

            $task = array();
            $task['id']                 = isset($_POST['id']) ?   $_POST['id']  :NULL;
            $task['title']              = isset($_POST['title']) ?   $_POST['title']  :NULL;
            $task['long_description']   = isset($_POST['long_description']) ?   $_POST['long_description']  :NULL;
            $task['is_done']            = isset($_POST['is_done']) ?   $_POST['is_done']  :0;
            $task['finish_date']        = !empty($_POST['finish_date'])?   $_POST['finish_date']  :NULL;
            $task['created_date']       = !empty($_POST['created_date']) ?   $_POST['created_date']  :date('Y-m-d H:i:s');
            $task['due_date']           = !empty($_POST['due_date']) ?   $_POST['due_date']  :NULL;
            
            
            try {
                if($action == 'edit'){
                    $this->taskService->updateTask($task);
                    $idRetun = $task['id'];
                }else if($action== 'new'){
                    unset($task['id']);
                    $idRetun = $this->taskService->createTask($task);
                }

                $this->redirect(BASEURL.'?action=edit&task='.$idRetun.'&save=success');
                return;
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
            }
        }

        include 'view/task/task.edit.php';

    }

    /**
     * Deleta uma tarefa
     */
    public function deleteTask() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        
        $this->taskService->deleteTask($id);
        
        $this->redirect(BASEURL);
    }

    /**
     * Finaliza uma tarefa
     */
    public function finishTask() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }

        $task = $this->taskService->getTask($id);

        $task['is_done'] = 1;
        $task['due_date'] = date('Y-m-d H:i:s');
        
        $this->taskService->updateTask($task);
        
        $this->redirect(BASEURL.'?action=list&status=closed');
    }

    /**
     * Reabre uma tarefa
     */
    public function reopenTask() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }

        $task = $this->taskService->getTask($id);

        $task['is_done'] = 0;
        $task['due_date'] = NULL;
        
        $this->taskService->updateTask($task);
        
        $this->redirect(BASEURL);
    }

    public function showError($title, $message) {
        include 'view/error.php';
    }

}


?>
