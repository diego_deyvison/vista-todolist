<?php

require_once 'controller/TaskController.php';
require_once 'config.php';

$controller = new TaskController();

$controller->handleRequest();

?>